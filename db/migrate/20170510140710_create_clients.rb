class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :client_organization_name
      t.string :client_email
      t.string :client_organization_person_name
      t.string :client_organization_phone
      t.string :client_personal_mobile
      t.string :client_organization_street_address1
      t.string :client_organization_street_address2
      t.string :client_organization_city
      t.string :internal_notes

      t.timestamps
    end
  end
end
