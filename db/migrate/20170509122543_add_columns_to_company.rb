class AddColumnsToCompany < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :company_contact_person, :string
    add_column :companies, :contact_person_title, :string
    add_column :companies, :company_city, :string
    add_column :companies, :company_street_address1, :string
    add_column :companies, :company_street_address2, :string
    add_column :companies, :company_state, :string
    add_column :companies, :company_city_zipcode, :string
    add_column :companies, :company_phone_number, :string
    add_column :companies, :company_email, :string
    add_column :companies, :company_cin, :string
    add_column :companies, :company_vat, :string
    add_column :companies, :company_bank_info, :string
  end
end
