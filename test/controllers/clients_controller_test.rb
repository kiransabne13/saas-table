require 'test_helper'

class ClientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client = clients(:one)
  end

  test "should get index" do
    get clients_url
    assert_response :success
  end

  test "should get new" do
    get new_client_url
    assert_response :success
  end

  test "should create client" do
    assert_difference('Client.count') do
      post clients_url, params: { client: { client_email: @client.client_email, client_organization_city: @client.client_organization_city, client_organization_name: @client.client_organization_name, client_organization_person_name: @client.client_organization_person_name, client_organization_phone: @client.client_organization_phone, client_organization_street_address1: @client.client_organization_street_address1, client_organization_street_address2: @client.client_organization_street_address2, client_personal_mobile: @client.client_personal_mobile, internal_notes: @client.internal_notes } }
    end

    assert_redirected_to client_url(Client.last)
  end

  test "should show client" do
    get client_url(@client)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_url(@client)
    assert_response :success
  end

  test "should update client" do
    patch client_url(@client), params: { client: { client_email: @client.client_email, client_organization_city: @client.client_organization_city, client_organization_name: @client.client_organization_name, client_organization_person_name: @client.client_organization_person_name, client_organization_phone: @client.client_organization_phone, client_organization_street_address1: @client.client_organization_street_address1, client_organization_street_address2: @client.client_organization_street_address2, client_personal_mobile: @client.client_personal_mobile, internal_notes: @client.internal_notes } }
    assert_redirected_to client_url(@client)
  end

  test "should destroy client" do
    assert_difference('Client.count', -1) do
      delete client_url(@client)
    end

    assert_redirected_to clients_url
  end
end
