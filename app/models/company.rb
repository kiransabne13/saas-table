class Company < ApplicationRecord

  validates :company_name, presence: true, uniqueness: true
  validates :company_contact_person, :company_city, :company_street_address1, :company_street_address2, :company_state, :company_phone_number, :company_email, presence: true
end
